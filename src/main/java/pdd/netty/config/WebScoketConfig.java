package pdd.netty.config;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.codec.http.websocketx.extensions.compression.WebSocketServerCompressionHandler;
import io.netty.handler.ssl.SslContext;
import pdd.netty.Handler.HttpHandler;
import pdd.netty.Handler.WebScoketFramHadler;

/**
 * @author:liyangpeng
 * @date:2020/1/14 14:19
 */
public class WebScoketConfig extends ChannelInitializer<SocketChannel> {

    private SslContext sslContext;

    public WebScoketConfig(SslContext sslContext){
        this.sslContext=sslContext;
    }

    protected void initChannel(SocketChannel socketChannel) throws Exception {
        ChannelPipeline pipeline = socketChannel.pipeline();
        if(this.sslContext!=null){
            pipeline.addLast(sslContext.newHandler(socketChannel.alloc()));
        }
        pipeline.addLast(new HttpServerCodec());
        pipeline.addLast(new HttpObjectAggregator(65536));
        pipeline.addLast(new WebSocketServerCompressionHandler());
        pipeline.addLast(new WebSocketServerProtocolHandler("/", null, true));
        pipeline.addLast(new HttpHandler());
        pipeline.addLast(new WebScoketFramHadler());
    }
}
